# Ash's Personal README

## About Me

- Engineer at GitLab since April 2018
- [Ruby](https://www.ruby-lang.org/en/) and [Go](https://go.dev/) enthusiast
- Big into [Home Assistant](https://www.home-assistant.io/) and [Node-RED](https://nodered.org/)
- Love [Proxmox VE](https://www.proxmox.com/en/proxmox-ve) and [Synology NAS devices](https://www.synology.com/en-au/products?tower=ds_j%2Cds_plus%2Cds_value%2Cds_xs)
- Star Wars nerd
- Avid console video gamer with favourite games:
  - [Red Dead Redemption 1 & 2](https://en.wikipedia.org/wiki/Red_Dead)
  - [Super Mario 3D World](https://en.wikipedia.org/wiki/Super_Mario_3D_World)
  - [Control](https://en.wikipedia.org/wiki/Control_(video_game))
  - [Grand Theft Auto V](https://en.wikipedia.org/wiki/Grand_Theft_Auto_V)
  - [Ghost of Tsushima](https://en.wikipedia.org/wiki/Ghost_of_Tsushima)
  - [Astro Bot](https://en.wikipedia.org/wiki/Astro_Bot)
  - [Days Gone](https://en.wikipedia.org/wiki/Days_Gone)
- Hack tennis player, I'd be on the court right now if I could 😉

## Links

- [My GitLab profile](https://gitlab.com/ashmckenzie)
- [My GitHub profile](https://github.com/ashmckenzie)

You can learn more about what I do for a job in my [professional README](https://gitlab.com/ashmckenzie/ashmckenzie).
